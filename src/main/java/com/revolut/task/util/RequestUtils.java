package com.revolut.task.util;

import com.revolut.task.rest.Path;
import java.math.BigDecimal;
import org.apache.commons.lang3.math.NumberUtils;
import spark.Request;

public class RequestUtils {

    public static boolean isAmountPositive(final BigDecimal balance) {
        return balance.compareTo(BigDecimal.ZERO) >= 0;
    }

    public static Long extractRequestParamAsLong(final Request request) {
        return NumberUtils.createLong(request.params().get(Path.ID_PARAM));
    }
}
