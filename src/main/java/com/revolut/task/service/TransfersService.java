package com.revolut.task.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hazelcast.query.impl.predicates.EqualPredicate;
import com.hazelcast.query.impl.predicates.OrPredicate;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transfer;
import com.revolut.task.repository.Repository;
import com.revolut.task.util.RequestUtils;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

public class TransfersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TransfersService.class);
    private static final String ATTRIBUTE_SENDER_ID = "senderId";
    private static final String ATTRIBUTE_RECEIVER_ID = "receiverId";

    private final Repository<String, Transfer> transfersStore;
    private final Repository<Long, Account> accountsStore;
    private final Gson gson;
    private final Lock lock;

    public TransfersService(final Repository<String, Transfer> transfersStore,
        final Repository<Long, Account> accountsStore) {
        this.transfersStore = transfersStore;
        this.accountsStore = accountsStore;
        gson = new GsonBuilder().create();
        lock = new ReentrantLock(true);
    }

    public Object handleTransferRequest(final Request request, final Response response) {
        lock.lock();
        try {
            Optional.ofNullable(gson.fromJson(request.body(), Transfer.class))
                .filter(it -> receiverAndSenderNotSame(it.getSenderId(), it.getReceiverId()))
                .filter(it -> RequestUtils.isAmountPositive(it.getAmount()))
                .filter(this::doTransfer)
                .ifPresentOrElse(it -> {
                    response.body("Transfer was successful");
                    response.type("text/plain");
                    response.status(200);
                }, () -> {
                    LOGGER.warn("Transfer cannot be executed, sender/receiver does not exist or insufficient funds");
                    response.body("Transfer request cannot be executed");
                    response.type("text/plain");
                    response.status(400);
                });
        } catch (Exception e) {
            LOGGER.warn("Error occurred while transferring money: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
        return response.body();
    }

    public Object getTransfersHistoryByAccountId(final Request request, final Response response) {
        lock.lock();
        try {
            final List<Transfer> transfers = transfersStore.values(buildTransfersPredicate(RequestUtils.extractRequestParamAsLong(request)));
            response.body(gson.toJson(transfers));
            response.type("application/json");
            response.status(200);
        } catch (Exception e) {
            LOGGER.warn("Error occurred while getting transfer history: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
        return response.body();
    }

    private OrPredicate buildTransfersPredicate(final Long accountId) {
        return new OrPredicate(
            new EqualPredicate(ATTRIBUTE_SENDER_ID, accountId),
            new EqualPredicate(ATTRIBUTE_RECEIVER_ID, accountId)
        );
    }

    private boolean receiverAndSenderNotSame(final Long senderId, final Long receiverId) {
        return !Objects.equals(senderId, receiverId);
    }

    private boolean doTransfer(final Transfer transfer) {
        final Optional<Account> optionalSender = Optional.ofNullable(accountsStore.get(transfer.getSenderId()));
        final Optional<Account> optionalReceiver = Optional.ofNullable(accountsStore.get(transfer.getReceiverId()));

        Optional<Pair<Account, Account>> pairOptional = optionalSender
            .filter(sender -> sender.getBalance().compareTo(transfer.getAmount()) >= 0)
            .flatMap(sender -> optionalReceiver
                .map(receiver -> new ImmutablePair<>(sender, receiver))
            );

        if (pairOptional.isPresent()) {
            var pair = pairOptional.get();
            final Account sender = pair.getLeft();
            final Account receiver = pair.getRight();
            sender.setBalance(sender.getBalance().subtract(transfer.getAmount()));
            accountsStore.putTransactional(sender.getId(), sender);
            receiver.setBalance(receiver.getBalance().add(transfer.getAmount()));
            accountsStore.putTransactional(receiver.getId(), receiver);
            transfersStore.putTransactional(transfer.getId(), transfer);
            return true;
        }
        return false;
    }
}
