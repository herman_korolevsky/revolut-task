package com.revolut.task.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.task.model.Account;
import com.revolut.task.repository.Repository;
import com.revolut.task.util.RequestUtils;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

public class AccountsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountsService.class);

    private final Repository<Long, Account> repository;
    private final Gson gson;
    private final ReentrantReadWriteLock readWriteLock;

    public AccountsService(final Repository<Long, Account> repository) {
        this.repository = repository;
        gson = new GsonBuilder().create();
        readWriteLock = new ReentrantReadWriteLock(true);
    }

    public Object getAccounts(final Request ignored, final Response response) {
        final ReadLock readLock = readWriteLock.readLock();
        readLock.lock();
        try {
            response.body(gson.toJson(repository.values()));
            response.type("application/json");
            response.status(200);
        } catch (Exception e) {
            LOGGER.warn("Error occurred while getting accounts: {}", e.getMessage());
        } finally {
            readLock.unlock();
        }
        return response.body();
    }

    public Object getAccountById(final Request request, final Response response) {
        final ReadLock readLock = readWriteLock.readLock();
        readLock.lock();
        try {
            Optional.ofNullable(RequestUtils.extractRequestParamAsLong(request))
                .map(repository::get)
                .map(gson::toJson)
                .ifPresentOrElse(body -> {
                    response.body(body);
                    response.type("application/json");
                    response.status(200);
                }, () -> {
                    LOGGER.warn("Account does not exist or cannot be serialized");
                    response.body("Account not found");
                    response.type("text/plain");
                    response.status(404);
                });
        } catch (Exception e) {
            LOGGER.warn("Error occurred while getting account: {}", e.getMessage());
        } finally {
            readLock.unlock();
        }
        return response.body();
    }

    public Object updateAccount(final Request request, final Response response) {
        final WriteLock writeLock = readWriteLock.writeLock();
        writeLock.lock();
        try {
            final Optional<Account> optionalAccount = Optional.ofNullable(gson.fromJson(request.body(), Account.class))
                .filter(account -> repository.containsKey(account.getId()))
                .filter(account -> RequestUtils.isAmountPositive(account.getBalance()));
            final Optional<Long> optionalId = Optional.ofNullable(RequestUtils.extractRequestParamAsLong(request));

            if (optionalAccount.isEmpty() || optionalId.isEmpty() ||
                !Objects.equals(optionalAccount.get().getId(), optionalId.get())) {
                LOGGER.warn("Account cannot be updated, body or id cannot be deserialized");
                response.body("Account cannot be updated");
                response.type("text/plain");
                response.status(400);
                return response.body();
            }

            repository.putTransactional(optionalId.get(), optionalAccount.get());
            response.body("Account was updated");
            response.type("text/plain");
            response.status(200);
        } catch (Exception e) {
            LOGGER.warn("Error occurred while updating account: {}", e.getMessage());
        } finally {
            writeLock.unlock();
        }
        return response.body();
    }

    public Object createAccount(final Request request, final Response response) {
        final WriteLock writeLock = readWriteLock.writeLock();
        writeLock.lock();
        try {
            Optional.ofNullable(gson.fromJson(request.body(), Account.class))
                .filter(account -> !repository.containsKey(account.getId()))
                .filter(account -> RequestUtils.isAmountPositive(account.getBalance()))
                .ifPresentOrElse(account -> {
                        repository.putTransactional(account.getId(), account);
                        response.body("Account was created");
                        response.type("text/plain");
                        response.status(200);
                    },
                    () -> {
                        LOGGER.warn("Account cannot be created, body cannot be deserialized");
                        response.body("Account cannot be created");
                        response.type("text/plain");
                        response.status(400);
                    });
        } catch (Exception e) {
            LOGGER.warn("Error occurred while creating account: {}", e.getMessage());
        } finally {
            writeLock.unlock();
        }
        return response.body();
    }

    public Object deleteAccount(final Request request, final Response response) {
        final WriteLock writeLock = readWriteLock.writeLock();
        writeLock.lock();
        try {
            Optional.ofNullable(RequestUtils.extractRequestParamAsLong(request))
                .map(repository::get)
                .ifPresentOrElse(account -> {
                        repository.removeTransactional(account.getId(), account);
                        response.body("Account was removed");
                        response.type("text/plain");
                        response.status(200);
                    },
                    () -> {
                        LOGGER.warn("Nothing to remove");
                        response.body("Account cannot be removed");
                        response.type("text/plain");
                        response.status(400);
                    });
        } catch (Exception e) {
            LOGGER.warn("Error occurred while deleting account: {}", e.getMessage());
        } finally {
            writeLock.unlock();
        }
        return response.body();
    }
}
