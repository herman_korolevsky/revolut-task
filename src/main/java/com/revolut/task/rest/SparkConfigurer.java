package com.revolut.task.rest;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.put;

import com.revolut.task.datastore.HazelcastConfigurer;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transfer;
import org.apache.commons.lang3.StringUtils;
import com.revolut.task.repository.AccountsRepository;
import com.revolut.task.repository.Repository;
import com.revolut.task.repository.TransfersRepository;
import com.revolut.task.service.AccountsService;
import com.revolut.task.service.TransfersService;

public final class SparkConfigurer {

    private static final int SERVER_PORT = 8080;

    private SparkConfigurer() {
    }

    public static void configureRestEndpoints() {
        final Repository<Long, Account> accountsStore = new AccountsRepository(HazelcastConfigurer.getHazelcastInstance());
        final Repository<String, Transfer> transfersStore = new TransfersRepository(HazelcastConfigurer.getHazelcastInstance());
        final AccountsService accountsService = new AccountsService(accountsStore);
        final TransfersService transfersService = new TransfersService(transfersStore, accountsStore);

        port(SERVER_PORT);
        path(Path.ROOT_API, () -> {
            path(Path.ACCOUNTS, () -> {
                get(Path.SLASH, accountsService::getAccounts);
                get(StringUtils.join(Path.SLASH, Path.ID_PARAM), accountsService::getAccountById);
                put(StringUtils.join(Path.SLASH, Path.ID_PARAM), accountsService::updateAccount);
                post(Path.SLASH, accountsService::createAccount);
                delete(StringUtils.join(Path.SLASH, Path.ID_PARAM), accountsService::deleteAccount);
            });

            path(Path.TRANSFERS, () -> {
                get(StringUtils.join(Path.SLASH, Path.ID_PARAM), transfersService::getTransfersHistoryByAccountId);
                post(Path.SLASH, transfersService::handleTransferRequest);
            });
        });
    }
}
