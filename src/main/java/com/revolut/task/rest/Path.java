package com.revolut.task.rest;

public final class Path {

    private Path() {
    }

    public static final String ROOT_API = "/api";
    public static final String ACCOUNTS = "/accounts";
    public static final String TRANSFERS = "/transfers";
    public static final String ID_PARAM = ":id";
    public static final String SLASH = "/";
}
