package com.revolut.task.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

public class Transfer implements Serializable {

    private final String id = UUID.randomUUID().toString();
    private Long senderId;
    private Long receiverId;
    private BigDecimal amount;

    public String getId() {
        return id;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(final Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(final Long receiverId) {
        this.receiverId = receiverId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(final BigDecimal amount) {
        this.amount = amount;
    }
}
