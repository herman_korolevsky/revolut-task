package com.revolut.task.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Account implements Serializable {

    private Long id;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(final BigDecimal balance) {
        this.balance = balance;
    }
}
