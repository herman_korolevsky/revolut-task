package com.revolut.task.repository;

import com.hazelcast.core.HazelcastInstance;
import com.revolut.task.model.Account;

public class AccountsRepository extends AbstractRepository<Long, Account> {

    private static final String ACCOUNTS_STORE = "accounts";

    public AccountsRepository(final HazelcastInstance hazelcastInstance) {
        super(hazelcastInstance, ACCOUNTS_STORE);
    }
}
