package com.revolut.task.repository;

import com.hazelcast.core.HazelcastInstance;
import com.revolut.task.model.Transfer;

public class TransfersRepository extends AbstractRepository<String, Transfer> {

    private static final String TRANSFERS_STORE = "transfers";

    public TransfersRepository(final HazelcastInstance hazelcastInstance) {
        super(hazelcastInstance, TRANSFERS_STORE);
    }
}
