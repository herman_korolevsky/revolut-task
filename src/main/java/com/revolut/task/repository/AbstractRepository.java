package com.revolut.task.repository;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.Predicate;
import com.hazelcast.transaction.TransactionContext;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractRepository<K, V> implements Repository<K, V> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractRepository.class);
    private final HazelcastInstance instance;
    private final IMap<K, V> store;

    AbstractRepository(final HazelcastInstance instance, final String storeName) {
        this.instance = instance;
        store = instance.getMap(storeName);
    }

    @Override
    public void putTransactional(final K key, final V value) {
        final TransactionContext transactionContext = instance.newTransactionContext();
        transactionContext.beginTransaction();
        try {
            store.put(key, value);
            transactionContext.commitTransaction();
        } catch (Exception e) {
            LOGGER.error("Transaction failed, rolling back. Message: {}", e.getMessage());
            transactionContext.rollbackTransaction();
        }
    }

    @Override
    public void removeTransactional(final K key, final V value) {
        final TransactionContext transactionContext = instance.newTransactionContext();
        transactionContext.beginTransaction();
        try {
            store.remove(key, value);
            transactionContext.commitTransaction();
        } catch (Exception e) {
            LOGGER.error("Transaction failed, rolling back. Message: {}", e.getMessage());
            transactionContext.rollbackTransaction();
        }
    }

    @Override
    public V get(final K key) {
        return store.get(key);
    }

    @Override
    public boolean containsKey(final K key) {
        return store.containsKey(key);
    }

    @Override
    public List<V> values() {
        return new ArrayList<>(store.values());
    }

    @Override
    public List<V> values(final Predicate predicate) {
        return new ArrayList<>(store.values(predicate));
    }
}
