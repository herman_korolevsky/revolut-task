package com.revolut.task.repository;

import com.hazelcast.query.Predicate;
import java.util.List;

public interface Repository<K, V> {

    void putTransactional(K key, V value);

    void removeTransactional(K key, V value);

    V get(K key);

    boolean containsKey(K key);

    List<V> values();

    List<V> values(Predicate predicate);
}
