package com.revolut.task;

import com.revolut.task.rest.SparkConfigurer;

public class TransferApp {

    public static void main(String[] args) {
        SparkConfigurer.configureRestEndpoints();
    }
}
