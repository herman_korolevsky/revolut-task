package com.revolut.task.datastore;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import java.util.UUID;

public final class HazelcastConfigurer {

    private static HazelcastInstance instance;

    private HazelcastConfigurer() {
    }

    static {
        instance = Hazelcast.newHazelcastInstance(new Config(UUID.randomUUID().toString()));
    }

    public static synchronized HazelcastInstance getHazelcastInstance() {
        if (instance == null) {
            instance = Hazelcast.newHazelcastInstance(new Config(UUID.randomUUID().toString()));
        }
        return instance;
    }

}
