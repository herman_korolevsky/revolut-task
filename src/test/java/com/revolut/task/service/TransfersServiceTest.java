package com.revolut.task.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.google.gson.GsonBuilder;
import com.hazelcast.query.Predicate;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transfer;
import com.revolut.task.repository.AccountsRepository;
import com.revolut.task.repository.TransfersRepository;
import com.revolut.task.rest.Path;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import spark.Request;
import spark.Response;

class TransfersServiceTest {

    private TransfersService service;
    @Mock
    AccountsRepository accountsRepository;
    @Mock
    TransfersRepository transfersRepository;
    @Mock
    Request request;
    @Mock
    Response response;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        service = new TransfersService(transfersRepository, accountsRepository);
    }

    @Test
    void handleTransferRequestTest() {
        final String body = "{\"senderId\":1,\"receiverId\":2,\"amount\":3}";
        final Account receiver = createAccount(2L, 5);
        final Account sender = createAccount(1L, 5);

        when(request.body()).thenReturn(body);
        when(accountsRepository.containsKey(2L)).thenReturn(true);
        when(accountsRepository.get(2L)).thenReturn(receiver);
        when(accountsRepository.containsKey(1L)).thenReturn(true);
        when(accountsRepository.get(1L)).thenReturn(sender);

        service.handleTransferRequest(request, response);

        verify(accountsRepository, times(1)).get(1L);
        verify(accountsRepository, times(1)).get(2L);
        verify(accountsRepository, times(1)).putTransactional(1L, sender);
        verify(accountsRepository, times(1)).putTransactional(2L, receiver);
        verify(response, times(1)).body("Transfer was successful");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(200);

        assertEquals(BigDecimal.valueOf(8), receiver.getBalance());
        assertEquals(BigDecimal.valueOf(2), sender.getBalance());
    }

    @Test
    void failedTransferRequestSameAccountTest() {
        final String body = "{\"senderId\":1,\"receiverId\":1,\"amount\":3}";
        when(request.body()).thenReturn(body);

        service.handleTransferRequest(request, response);

        verify(response, times(1)).body("Transfer request cannot be executed");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
    }

    @Test
    void failedToCheckSenderTransferRequestTest() {
        final String body = "{\"senderId\":1,\"receiverId\":2,\"amount\":3}";
        when(request.body()).thenReturn(body);
        when(accountsRepository.containsKey(1L)).thenReturn(false);

        service.handleTransferRequest(request, response);

        verify(response, times(1)).body("Transfer request cannot be executed");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
    }

    @Test
    void failedToCheckReceiverTransferRequestTest() {
        final String body = "{\"senderId\":1,\"receiverId\":2,\"amount\":3}";
        when(request.body()).thenReturn(body);
        when(accountsRepository.containsKey(1L)).thenReturn(true);
        when(accountsRepository.get(1L)).thenReturn(createAccount(1L, 5));
        when(accountsRepository.containsKey(2L)).thenReturn(false);

        service.handleTransferRequest(request, response);

        verify(response, times(1)).body("Transfer request cannot be executed");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
    }

    @Test
    void failedToHandleTransferRequestTest() {
        when(request.body()).thenReturn(null);

        service.handleTransferRequest(request, response);

        verify(response, times(1)).body("Transfer request cannot be executed");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
    }

    @Test
    void failedToHandleTransferRequestNegativeMoneyTest() {
        final String body = "{\"senderId\":1,\"receiverId\":2,\"amount\":-3}";
        when(request.body()).thenReturn(body);
        when(accountsRepository.containsKey(1L)).thenReturn(true);
        when(accountsRepository.get(1L)).thenReturn(createAccount(1L, 5));
        when(accountsRepository.containsKey(2L)).thenReturn(true);

        service.handleTransferRequest(request, response);

        verify(response, times(1)).body("Transfer request cannot be executed");
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
    }

    @Test
    void getTransferHistoryTest() {
        final Transfer transfer = new Transfer();
        transfer.setAmount(BigDecimal.valueOf(1));
        transfer.setReceiverId(1L);
        transfer.setSenderId(2L);
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn("1");
        when(transfersRepository.values(any(Predicate.class))).thenReturn(Collections.singletonList(transfer));

        service.getTransfersHistoryByAccountId(request, response);

        verify(response, times(1)).body(new GsonBuilder().create().toJson(Collections.singletonList(transfer)));
        verify(response, times(1)).type("application/json");
        verify(response, times(1)).status(200);
        verify(response, times(1)).body();

    }

    private Account createAccount(final long id, final int amount) {
        final Account account = new Account();
        account.setId(id);
        account.setBalance(BigDecimal.valueOf(amount));
        return account;
    }
}