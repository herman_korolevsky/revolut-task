package com.revolut.task.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import com.revolut.task.model.Account;
import com.revolut.task.repository.AccountsRepository;
import com.revolut.task.rest.Path;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import spark.Request;
import spark.Response;

class AccountsServiceTest {

    private AccountsService service;
    @Mock
    AccountsRepository repository;
    @Mock
    Request request;
    @Mock
    Response response;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        service = new AccountsService(repository);
    }

    @Test
    void getAllAccountsTest() {
        final String expected = "[{\"id\":1,\"balance\":5}]";
        when(repository.values()).thenReturn(Collections.singletonList(createAccount()));
        service.getAccounts(request, response);
        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("application/json");
        verify(response, times(1)).status(200);
        verify(response, times(1)).body();
        verifyZeroInteractions(request);

    }

    @Test
    void getAccountsByIdTest() {
        final String expected = "{\"id\":1,\"balance\":5}";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.get(1L)).thenReturn(createAccount());

        service.getAccountById(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("application/json");
        verify(response, times(1)).status(200);
        verify(response, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
    }

    @Test
    void getAccountsByIdNotFoundTest() {
        final String expected = "Account not found";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.get(1L)).thenReturn(null);

        service.getAccountById(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(404);
        verify(response, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
    }

    @Test
    void createAccountTest() {
        final String expected = "Account was created";
        final String body = "{\"id\":1,\"balance\":5}";
        when(request.body()).thenReturn(body);
        when(repository.containsKey(1L)).thenReturn(false);

        service.createAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(200);
        verify(response, times(1)).body();
        verify(repository, times(1)).putTransactional(eq(1L), any(Account.class));
    }

    @Test
    void failedToCreateAccountEmptyBodyTest() {
        final String expected = "Account cannot be created";
        when(request.body()).thenReturn(null);

        service.createAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(response, times(1)).body();
        verifyZeroInteractions(repository);
    }

    @Test
    void failedToCreateAccountAlreadyExistsTest() {
        final String expected = "Account cannot be created";
        final String body = "{\"id\":1,\"balance\":5}";
        when(request.body()).thenReturn(body);
        when(repository.containsKey(1L)).thenReturn(true);

        service.createAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(response, times(1)).body();
        verify(repository, times(1)).containsKey(1L);
    }

    @Test
    void failedToCreateAccountNegativeMoneyTest() {
        final String expected = "Account cannot be created";
        final String body = "{\"id\":1,\"balance\":-5}";
        when(request.body()).thenReturn(body);
        when(repository.containsKey(1L)).thenReturn(false);

        service.createAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(response, times(1)).body();
        verify(repository, times(1)).containsKey(1L);
    }

    @Test
    void updateAccountTest() {
        final String expected = "Account was updated";
        final String body = "{\"id\":1,\"balance\":5}";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.containsKey(1L)).thenReturn(true);
        when(request.body()).thenReturn(body);

        service.updateAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(200);
        verify(request, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
        verify(repository, times(1)).containsKey(1L);
        verify(repository, times(1)).putTransactional(eq(1L), any(Account.class));
    }

    @Test
    void failedToUpdateAccountEmptyBodyTest() {
        final String expected = "Account cannot be updated";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.containsKey(1L)).thenReturn(true);
        when(request.body()).thenReturn(null);

        service.updateAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(request, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
        verifyZeroInteractions(repository);
    }

    @Test
    void failedToUpdateAccountNotExistsTest() {
        final String expected = "Account cannot be updated";
        final String body = "{\"id\":1,\"balance\":5}";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.containsKey(1L)).thenReturn(false);
        when(request.body()).thenReturn(body);

        service.updateAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(request, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
        verify(repository, times(1)).containsKey(1L);
    }

    @Test
    void failedToUpdateAccountNegativeMoneyTest() {
        final String expected = "Account cannot be updated";
        final String body = "{\"id\":1,\"balance\":-5}";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.containsKey(1L)).thenReturn(true);
        when(request.body()).thenReturn(body);

        service.updateAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(request, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
        verify(repository, times(1)).containsKey(1L);
    }

    @Test
    void deleteAccountTest() {
        final String expected = "Account was removed";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.get(1L)).thenReturn(createAccount());

        service.deleteAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(200);
        verify(response, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
    }

    @Test
    void failedToDeleteAccountTest() {
        final String expected = "Account cannot be removed";
        final String requestParam = "1";
        final Map<String, String> params = Mockito.mock(Map.class);
        when(request.params()).thenReturn(params);
        when(params.get(Path.ID_PARAM)).thenReturn(requestParam);
        when(repository.get(1L)).thenReturn(null);

        service.deleteAccount(request, response);

        verify(response, times(1)).body(expected);
        verify(response, times(1)).type("text/plain");
        verify(response, times(1)).status(400);
        verify(response, times(1)).body();
        verify(request, times(1)).params();
        verify(params, times(1)).get(Path.ID_PARAM);
    }

    private Account createAccount() {
        final Account account = new Account();
        account.setId(1L);
        account.setBalance(BigDecimal.valueOf(5));
        return account;
    }
}