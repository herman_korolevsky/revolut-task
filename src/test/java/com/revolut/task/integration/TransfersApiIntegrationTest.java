package com.revolut.task.integration;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;
import static spark.Spark.stop;

import com.revolut.task.datastore.HazelcastConfigurer;
import com.revolut.task.model.Account;
import com.revolut.task.model.Transfer;
import com.revolut.task.rest.SparkConfigurer;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.math.BigDecimal;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TransfersApiIntegrationTest {

    @BeforeAll
    static void setupAll() {
        stop();
        awaitStop();
        SparkConfigurer.configureRestEndpoints();
        awaitInitialization();
        RestAssured.basePath = "/api/transfers";
    }

    @BeforeEach
    void setupEach() {
        final Account sender = new Account();
        sender.setId(1L);
        sender.setBalance(BigDecimal.valueOf(5));

        final Account receiver = new Account();
        receiver.setId(2L);
        receiver.setBalance(BigDecimal.valueOf(5));

        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").put(sender.getId(), sender);
        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").put(receiver.getId(), receiver);
    }

    @AfterAll
    static void teardownAll() {
        stop();
    }

    @AfterEach
    void teardownEach() {
        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").clear();
        HazelcastConfigurer.getHazelcastInstance().getMap("transfers").clear();
    }

    @Test
    void sendTransferRequestTest() {
        final String responseBody = RestAssured
            .given()
                .body("{\"senderId\":1,\"receiverId\":2,\"amount\":5}")
            .when()
                .post("/")
            .then()
                .statusCode(200)
                .contentType(ContentType.TEXT)
            .extract()
                .body().asString();

        assertEquals("Transfer was successful", responseBody);
    }

}
