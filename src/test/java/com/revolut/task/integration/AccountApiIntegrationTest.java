package com.revolut.task.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static spark.Spark.awaitInitialization;
import static spark.Spark.awaitStop;
import static spark.Spark.stop;

import com.revolut.task.datastore.HazelcastConfigurer;
import com.revolut.task.model.Account;
import com.revolut.task.rest.SparkConfigurer;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import java.math.BigDecimal;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AccountApiIntegrationTest {

    @BeforeAll
    static void setupAll() {
        stop();
        awaitStop();
        SparkConfigurer.configureRestEndpoints();
        awaitInitialization();
        RestAssured.basePath = "/api/accounts";
    }

    @BeforeEach
    void setupEach() {
        final Account account = new Account();
        account.setId(1L);
        account.setBalance(BigDecimal.valueOf(5));
        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").put(account.getId(), account);
    }

    @AfterAll
    static void teardownAll() {
        stop();
    }

    @AfterEach
    void teardownEach() {
        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").clear();
    }

    @Test
    void getAllAccounts() {
        final String responseBody = RestAssured
            .when()
                .get("/")
            .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
            .extract()
                .body().asString();

        assertEquals("[{\"id\":1,\"balance\":5}]", responseBody);
    }

    @Test
    void getAccountById(){
        final String responseBody = RestAssured
            .given()
                .pathParam("id", 1)
            .when()
                .get("/{id}")
            .then()
                .statusCode(200)
                .contentType(ContentType.JSON)
            .extract()
                .body().asString();

        assertEquals("{\"id\":1,\"balance\":5}", responseBody);
    }

    @Test
    void updateAccount(){
        final String responseBody = RestAssured
            .given()
                .pathParam("id", 1)
                .body("{\"id\":1,\"balance\":6}")
            .when()
                .put("/{id}")
            .then()
                .statusCode(200)
                .contentType(ContentType.TEXT)
            .extract()
                .body().asString();

        assertEquals("Account was updated", responseBody);
    }

    @Test
    void createAccountById(){
        HazelcastConfigurer.getHazelcastInstance().getMap("accounts").clear();

        final String responseBody = RestAssured
            .given()
                .body("{\"id\":1,\"balance\":6}")
            .when()
                .post("/")
            .then()
                .statusCode(200)
                .contentType(ContentType.TEXT)
            .extract()
                .body().asString();

        assertEquals("Account was created", responseBody);
    }

    @Test
    void deleteAccountById(){
        final String responseBody = RestAssured
            .given()
                .pathParam("id", 1)
            .when()
                .delete("/{id}")
            .then()
                .statusCode(200)
                .contentType(ContentType.TEXT)
            .extract()
                .body().asString();

        assertEquals("Account was removed", responseBody);
    }
}
