package com.revolut.task.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.impl.predicates.AndPredicate;
import com.hazelcast.transaction.TransactionContext;
import com.revolut.task.model.Account;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class AccountsRepositoryTest {

    private AccountsRepository repository;
    @Mock
    HazelcastInstance instance;
    @Mock
    IMap accountsStore;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        when(instance.getMap("accounts")).thenReturn(accountsStore);
        repository = new AccountsRepository(instance);
    }

    @Test
    void putTransactionalTest() {
        final Long key = 1L;
        final Account value = new Account();
        TransactionContext context = mock(TransactionContext.class);
        when(instance.newTransactionContext()).thenReturn(context);

        repository.putTransactional(key, value);

        verify(accountsStore, times(1)).put(key, value);
        verify(instance, times(1)).newTransactionContext();
    }

    @Test
    void removeTransactionalTest() {
        final Long key = 1L;
        final Account value = new Account();
        TransactionContext context = mock(TransactionContext.class);
        when(instance.newTransactionContext()).thenReturn(context);

        repository.removeTransactional(key, value);

        verify(accountsStore, times(1)).remove(key, value);
        verify(instance, times(1)).newTransactionContext();
    }

    @Test
    void getTest() {
        final Long key = 1L;
        final Account value = new Account();
        when(accountsStore.get(key)).thenReturn(value);

        final Account result = repository.get(key);

        verify(accountsStore, times(1)).get(key);
        assertEquals(result, value);
    }

    @Test
    void containsKeyTest() {
        final Long key = 1L;
        when(accountsStore.containsKey(key)).thenReturn(true);

        final boolean result = repository.containsKey(key);

        verify(accountsStore, times(1)).containsKey(key);
        assertTrue(result);
    }

    @Test
    void valuesTest() {
        final List<Account> expected = Collections.singletonList(new Account());
        when(accountsStore.values()).thenReturn(expected);

        final List<Account> result = repository.values();

        verify(accountsStore, times(1)).values();
        assertEquals(expected, result);
    }

    @Test
    void valuesPredicateTest() {
        final AndPredicate predicate = new AndPredicate();
        final List<Account> expected = Collections.singletonList(new Account());
        when(accountsStore.values(predicate)).thenReturn(expected);

        final List<Account> result = repository.values(predicate);

        verify(accountsStore, times(1)).values(predicate);
        assertEquals(expected, result);
    }
}