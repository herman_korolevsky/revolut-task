package com.revolut.task.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.impl.predicates.AndPredicate;
import com.hazelcast.transaction.TransactionContext;
import com.revolut.task.model.Transfer;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TransfersRepositoryTest {

    private TransfersRepository repository;
    @Mock
    HazelcastInstance instance;
    @Mock
    IMap transfersStore;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(this);
        when(instance.getMap("transfers")).thenReturn(transfersStore);
        repository = new TransfersRepository(instance);
    }

    @Test
    void putTransactionalTest() {
        final String key = "1";
        final Transfer value = new Transfer();
        TransactionContext context = mock(TransactionContext.class);
        when(instance.newTransactionContext()).thenReturn(context);

        repository.putTransactional(key, value);

        verify(transfersStore, times(1)).put(key, value);
        verify(instance, times(1)).newTransactionContext();
    }

    @Test
    void removeTransactionalTest() {
        final String key = "1";
        final Transfer value = new Transfer();
        TransactionContext context = mock(TransactionContext.class);
        when(instance.newTransactionContext()).thenReturn(context);

        repository.removeTransactional(key, value);

        verify(transfersStore, times(1)).remove(key, value);
        verify(instance, times(1)).newTransactionContext();
    }

    @Test
    void getTest() {
        final String key = "1";
        final Transfer value = new Transfer();
        when(transfersStore.get(key)).thenReturn(value);

        final Transfer result = repository.get(key);

        verify(transfersStore, times(1)).get(key);
        assertEquals(result, value);
    }

    @Test
    void containsKeyTest() {
        final String key = "1";
        when(transfersStore.containsKey(key)).thenReturn(true);

        final boolean result = repository.containsKey(key);

        verify(transfersStore, times(1)).containsKey(key);
        assertTrue(result);
    }

    @Test
    void valuesTest() {
        final List<Transfer> expected = Collections.singletonList(new Transfer());
        when(transfersStore.values()).thenReturn(expected);

        final List<Transfer> result = repository.values();

        verify(transfersStore, times(1)).values();
        assertEquals(expected, result);
    }

    @Test
    void valuesPredicateTest() {
        final AndPredicate predicate = new AndPredicate();
        final List<Transfer> expected = Collections.singletonList(new Transfer());
        when(transfersStore.values(predicate)).thenReturn(expected);

        final List<Transfer> result = repository.values(predicate);

        verify(transfersStore, times(1)).values(predicate);
        assertEquals(expected, result);
    }
}