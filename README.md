# [Revolut Backend Test](https://drive.google.com/file/d/19IM0HmgQO_Tggke8wh7N764l67JFwMnq/view)
Design and implement a RESTful API (including data model and the backing implementation)
for money transfers between accounts.

## Explicit requirements:
1. You can use Java, Scala or Kotlin.
2. Keep it simple and to the point (e.g. no need to implement any authentication).
3. Assume the API is invoked by multiple systems and services on behalf of end users.
4. You can use frameworks/libraries if you like (except Spring), but don't forget about
requirement #2 – keep it simple and avoid heavy frameworks.
5. The datastore should run in-memory for the sake of this test.
6. The final result should be executable as a standalone program (should not require
a pre-installed container/server).
7. Demonstrate with tests that the API works as expected.

## Implicit requirements:
1. The code produced by you is expected to be of high quality.
2. There are no detailed requirements, use common sense.

**Please put your work on github or bitbucket.**

## Solution details:
Java 11 was used for implementation
Dependencies
   - Logback - for logging
   - Spark java - a simple web framework
   - Hazelcast - as in memory data store
   - Junit5 - for unit tests
   - Mockito - mocking framework for unit tests
   - Apache commons - util libs
   - Google gson - json lib
   - Rest Assured - rest api testing lib
5. Hazelcast is used to meet requirement #5, was chosen as quick in memory data store, 
supports transactions
6. Maven Assembly plugin is used to meet requirement #6.
7. See unit tests.

## How to run:
1. Run `mvn clean package` 
2. Run `java -jar target/transfer-app-1.0-jar-with-dependencies.jar`
3. find server running on `localhost:8080`

## API Reference
|     *URL*    | *HTTP method* |                        *Description*                               |
|---------------|---------------|--------------------------------------------------------------------|
|/api/accounts/ |      GET      |Returns all available accounts stored in memory|  
|/api/accounts/{id} |      GET      |Returns account with specific `{id}` stored in memory|  
|/api/accounts/{id} |      PUT      |Updates an account with specific `{id}`, body json is `{"id":4, "balance":5}`|  
|/api/accounts/ |      POST      |Creates an account, body json is `{"id":4, "balance":5}`|  
|/api/accounts/{id} |      DELETE      |Deletes an account with specific `{id}` from memory|  
|/api/transfers/{id} |      GET      |Returns a list of transfer records where `{id}` matches sender or receiver ids in transfers history|  
|/api/transfers/ |      POST      |Post a transfer request, fire and forget, request is executed in separate thread if possible, body json is `{"senderId":1,"receiverId":2,"amount":3}`|  